
var AreaCutoff = 150;
var GreenThreshold = 60;
var makeMasks = true;
var AllCellResults = true;
var dir1 = "C:\\";
var dir2 = "C:\\";
var headless = false;

run("Close All");
setBatchMode(headless);

if(!headless){
	//masterDir = "Y:\\Ken Pang\\Images for Lachie\\siRNA localisation\\";
	masterDir = getDirectory("Choose source directory");
}else{
	masterDir = getArgument();
}

run("Set Measurements...", "area mean area_fraction limit display redirect=None decimal=4");
run("Bio-Formats Macro Extensions");

dirList = getFileList(masterDir);
average_results = "Filename \t Number of cells \t Average Intensity \t Average Area \t Average Number\n"; //for results table
		

for(i=0;i<dirList.length;i++){
	if(File.isDirectory(masterDir+dirList[i])){
		dir1 = masterDir+dirList[i];
		dir2 = dir1+"output\\";
		if(!File.exists(dir2)){
			File.makeDirectory(dir2);
		}
		
		list = getFileList(dir1);
		
		for(j=0;j<list.length;j++){
			if(endsWith(list[j],".czi") || endsWith(list[j],".lsm")){
				fpath = dir1 + list[j];
				Ext.openImagePlus(fpath);
				result = processImage(list[j]);
				average_results = average_results + result;
				run("Close All");
			}
		}
	}
}

resultsFile = File.open(masterDir+"results.txt");
print(resultsFile,average_results);
File.close(resultsFile);
File.rename(masterDir+"results.txt",masterDir+"results.xls");



if(headless){quit();}

//Ext.setID(fpath);
function processImage(asdf){
	run("Z Project...", "projection=[Max Intensity]");
	fname = getTitle();
	Stack.setChannel(1);
	run("Enhance Contrast", "saturated=0.35");
	Stack.setChannel(2);
	run("Enhance Contrast", "saturated=0.35");
	run("Make Composite");
	run("RGB Color");
	rename("mask");
	selectWindow(fname);
	
	run("Split Channels");
	selectWindow("C2-"+fname);
	run("Median...","Radius=5");
	
	setThreshold(12,2255);
	run("Convert to Mask");
	run("Fill Holes");
	run("Watershed");
	
	run("Analyze Particles...", "size=0-Infinity pixel show=Overlay display clear");
	
	for(i=0;i<nResults();i++){
		a = getResult("Area",i);
		if(a<AreaCutoff){
			Overlay.activateSelection(i);
			run("Clear", "slice");
		}
	}
	
	run("Remove Overlay");
	setBackgroundColor(0,0,0);
	run("Select None");
	
	run("Voronoi");
	setThreshold(1,255);
	
	run("Convert to Mask");
	run("Invert");
	
	run("Analyze Particles...", "pixel show=Overlay display clear");
	Overlay.copy();
	selectWindow("C1-"+fname);
	Overlay.paste();
	nCells = nResults();
	if(nCells>100){
		print(nCells);
		
	}
	run("Clear Results");
	
	
	cellByCellResults = "Cell Identifier # \t Mean Intensity \t Area stained \t nDots\n";
	
	area_array = newArray();
	mean_array = newArray();
	nThings_array = newArray();
	
	
	
	for(i=0;i<nCells;i++){
		Overlay.activateSelection(i);
		setThreshold(GreenThreshold,255);
	
		run("Measure");
		a = getResult("Area");
		if(isNaN(a)){
			a = 0;
		}
		m = getResult("Mean");		
		if( isNaN(m) ){
			m = 0;
		}
		run("Analyze Particles...", "pixel show=Nothing display clear");
		nThings = nResults();
		area_array = Array.concat(area_array,a);
		mean_array = Array.concat(mean_array,m);
		nThings_array = Array.concat(nThings_array,nThings);
		cellByCellResults = cellByCellResults + i + " \t " + m + " \t " + a + " \t " + nThings + "\n";
	}
	
	Array.getStatistics(mean_array,blank,blank,mean_mean,blank);
	Array.getStatistics(area_array,blank,blank,mean_area,blank);
	Array.getStatistics(nThings_array,blank,blank,mean_nThings,blank);
	
	resultString = fname + " \t " + nCells + " \t " + mean_mean + " \t " + mean_area + " \t " + mean_nThings + "\n";
		
	
	if(AllCellResults){
		image_cell_file = File.open(dir2+asdf+"_cellByCellResults.txt");
		print(image_cell_file,cellByCellResults);
		File.close(image_cell_file);
		
		x = File.rename(dir2+asdf+"_cellByCellResults.txt",dir2+asdf+"_cellByCellResults.xls");
		x = File.delete(dir2+asdf+"_cellByCellResults.txt");
	}
	
	if(makeMasks){
		selectWindow("mask");
		Overlay.paste();
		Overlay.drawLabels(false);
		run("Flatten");
		saveAs("JPG",dir2+asdf+"_mask.jpg");
	}
	return resultString;
}

function quit(){
	eval("script", "System.exit(0);");
}


